/*
FILE:     htmlUtil.h
*/
/*
VERSION:  2.250
*/
/*
DATE:     10/21/2013
*/
/*
  Comments and Questions to: sw-help@rcsb.rutgers.edu
*/
/*
COPYRIGHT 1999-2013 Rutgers - The State University of New Jersey

This software is provided WITHOUT WARRANTY OF MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, EXPRESS OR
IMPLIED.  RUTGERS MAKE NO REPRESENTATION OR WARRANTY THAT THE
SOFTWARE WILL NOT INFRINGE ANY PATENT, COPYRIGHT OR OTHER
PROPRIETARY RIGHT.

The user of this software shall indemnify, hold harmless and defend
Rutgers, its governors, trustees, officers, employees, students,
agents and the authors against any and all claims, suits,
losses, liabilities, damages, costs, fees, and expenses including
reasonable attorneys' fees resulting from or arising out of the
use of this software.  This indemnification shall include, but is
not limited to, any and all claims alleging products liability.
*/
/*
               RCSB PDB SOFTWARE LICENSE AGREEMENT

BY CLICKING THE ACCEPTANCE BUTTON OR INSTALLING OR USING 
THIS "SOFTWARE, THE INDIVIDUAL OR ENTITY LICENSING THE  
SOFTWARE ("LICENSEE") IS CONSENTING TO BE BOUND BY AND IS 
BECOMING A PARTY TO THIS AGREEMENT.  IF LICENSEE DOES NOT 
AGREE TO ALL OF THE TERMS OF THIS AGREEMENT
THE LICENSEE MUST NOT INSTALL OR USE THE SOFTWARE.

1. LICENSE AGREEMENT

This is a license between you ("Licensee") and the Protein Data Bank (PDB) 
at Rutgers, The State University of New Jersey (hereafter referred to 
as "RUTGERS").   The software is owned by RUTGERS and protected by 
copyright laws, and some elements are protected by laws governing 
trademarks, trade dress and trade secrets, and may be protected by 
patent laws. 

2. LICENSE GRANT

RUTGERS grants you, and you hereby accept, non-exclusive, royalty-free 
perpetual license to install, use, modify, prepare derivative works, 
incorporate into other computer software, and distribute in binary 
and source code format, or any derivative work thereof, together with 
any associated media, printed materials, and on-line or electronic 
documentation (if any) provided by RUTGERS (collectively, the "SOFTWARE"), 
subject to the following terms and conditions: (i) any distribution 
of the SOFTWARE shall bind the receiver to the terms and conditions 
of this Agreement; (ii) any distribution of the SOFTWARE in modified 
form shall clearly state that the SOFTWARE has been modified from 
the version originally obtained from RUTGERS.  

2. COPYRIGHT; RETENTION OF RIGHTS.  

The above license grant is conditioned on the following: (i) you must 
reproduce all copyright notices and other proprietary notices on any 
copies of the SOFTWARE and you must not remove such notices; (ii) in 
the event you compile the SOFTWARE, you will include the copyright 
notice with the binary in such a manner as to allow it to be easily 
viewable; (iii) if you incorporate the SOFTWARE into other code, you 
must provide notice that the code contains the SOFTWARE and include 
a copy of the copyright notices and other proprietary notices.  All 
copies of the SOFTWARE shall be subject to the terms of this Agreement.  

3. NO MAINTENANCE OR SUPPORT; TREATMENT OF ENHANCEMENTS 

RUTGERS is under no obligation whatsoever to: (i) provide maintenance 
or support for the SOFTWARE; or (ii) to notify you of bug fixes, patches, 
or upgrades to the features, functionality or performance of the 
SOFTWARE ("Enhancements") (if any), whether developed by RUTGERS 
or third parties.  If, in its sole discretion, RUTGERS makes an 
Enhancement available to you and RUTGERS does not separately enter 
into a written license agreement with you relating to such bug fix, 
patch or upgrade, then it shall be deemed incorporated into the SOFTWARE 
and subject to this Agreement. You are under no obligation whatsoever 
to provide any Enhancements to RUTGERS or the public that you may 
develop over time; however, if you choose to provide your Enhancements 
to RUTGERS, or if you choose to otherwise publish or distribute your 
Enhancements, in source code form without contemporaneously requiring 
end users or RUTGERS to enter into a separate written license agreement 
for such Enhancements, then you hereby grant RUTGERS a non-exclusive,
royalty-free perpetual license to install, use, modify, prepare
derivative works, incorporate into the SOFTWARE or other computer
software, distribute, and sublicense your Enhancements or derivative
works thereof, in binary and source code form.

4. FEES.  There is no license fee for the SOFTWARE.  If Licensee
wishes to receive the SOFTWARE on media, there may be a small charge
for the media and for shipping and handling.  Licensee is
responsible for any and all taxes.

5. TERMINATION.  Without prejudice to any other rights, Licensor
may terminate this Agreement if Licensee breaches any of its terms
and conditions.  Upon termination, Licensee shall destroy all
copies of the SOFTWARE.

6. PROPRIETARY RIGHTS.  Title, ownership rights, and intellectual
property rights in the Product shall remain with RUTGERS.  Licensee 
acknowledges such ownership and intellectual property rights and will 
not take any action to jeopardize, limit or interfere in any manner 
with RUTGERS' ownership of or rights with respect to the SOFTWARE.  
The SOFTWARE is protected by copyright and other intellectual 
property laws and by international treaties.  Title and related 
rights in the content accessed through the SOFTWARE is the property 
of the applicable content owner and is protected by applicable law.  
The license granted under this Agreement gives Licensee no rights to such
content.

7. DISCLAIMER OF WARRANTY.  THE SOFTWARE IS PROVIDED FREE OF 
CHARGE, AND, THEREFORE, ON AN "AS IS" BASIS, WITHOUT WARRANTY OF 
ANY KIND, INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT 
IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE 
OR NON-INFRINGING.  THE ENTIRE RISK AS TO THE QUALITY AND 
PERFORMANCE OF THE SOFTWARE IS BORNE BY LICENSEE.  SHOULD THE 
SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, THE LICENSEE AND NOT 
LICENSOR ASSUMES THE ENTIRE COST OF ANY SERVICE AND REPAIR.  
THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF 
THIS AGREEMENT.  NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER 
EXCEPT UNDER THIS DISCLAIMER.

8. LIMITATION OF LIABILITY.  TO THE MAXIMUM EXTENT PERMITTED BY
APPLICABLE LAW,  IN NO EVENT WILL LICENSOR BE LIABLE FOR ANY 
INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING 
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, INCLUDING, 
WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK 
STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL 
OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE
POSSIBILITY THEREOF. 
*/


#include <string>

/*
 *   These flags are used by the navigation function... 
 */
#define F_TOP_MENU               0x000001
#define F_DICT_INFO              0x000002
#define F_CATEGORY_GROUP_MENU    0x000004
#define F_CATEGORY_MENU          0x000008
#define F_ITEM_MENU              0x000010
#define F_DATA_MENU              0x000020


/*
 *  Colors, font sizes and icons are selected according to viewer type..
 * 
 */

#define    HT_BG_COLOR           "#ffffff"

#define    HT_IMAGE_BAR          "bar-bluepurple.gif"
#define    HT_ICON_DEFAULT       "redball.gif"
#define    HT_ICON_DEFAULT1      "blueball.gif"

#define    HT_ICON_ALIAS         "ALIAS.gif"
#define    HT_ICON_CATEGORIES    "CATEGORIES.gif"
#define    HT_ICON_CATEGORY      "CATEGORY.gif"
#define    HT_ICON_CHILD         "CHILD.gif"
#define    HT_ICON_DATA          "DATA.gif"
#define    HT_ICON_DATATYPE      "DATATYPE.gif"
#define    HT_ICON_VDEFAULT      "DEFAULT.gif"
#define    HT_ICON_DEPENDENCY    "DEPENDENCY.gif"
#define    HT_ICON_DESCRIPTION   "DESCRIPTION.gif"
#define    HT_ICON_ENUMERATION   "ENUMERATION.gif"
#define    HT_ICON_EXAMPLE       "EXAMPLE.gif"
#define    HT_ICON_EXAMPLES      "EXAMPLES.gif"
#define    HT_ICON_GROUPS        "GROUPS.gif"
#define    HT_ICON_GROUP         "GROUP.gif"
#define    HT_ICON_ITEMS         "ITEMS.gif"
#define    HT_ICON_ITEM          "ITEM.gif"
#define    HT_ICON_KEY           "KEY.gif"
#define    HT_ICON_KEYS          "KEYS.gif"
#define    HT_ICON_MAN           "MAN.gif"
#define    HT_ICON_MIS           "MIS.gif"
#define    HT_ICON_PARENT        "PARENT.gif"
#define    HT_ICON_RANGE         "RANGE.gif"
#define    HT_ICON_RELATED       "RELATED.gif"
#define    HT_ICON_SUBCATEGORY   "SUBCATEGORY.gif"
#define    HT_ICON_TYPE          "TYPE.gif"
#define    HT_ICON_UNITS         "UNITS.gif"

#define    HT_BASE_FONT_SIZE            4
#define    HT_MENU_NAME_FONT_SIZE       6
#define    HT_MENU_ITEM_FONT_SIZE       5
#define    HT_LIST_ITEM_FONT_SIZE       4
#define    HT_TABLE_HEADER_FONT_SIZE    4
#define    HT_TABLE_ITEM_FONT_SIZE      4
#define    HT_NAVIGATE_FONT_SIZE        4
#define    HT_LABEL_FONT_SIZE           5

#define LF 10
#define CR 13


#define HT_MAX_LINE_LENGTH      1000
#define HT_MAX_URL_LENGTH       256



/*
 *  Prototypes ... 
 *
 */


void htput_title(FILE *fp,const char *title);
void htput_open_body(FILE *fp, const char *title);
void htput_close_body(FILE *fp);
void htput_separator(FILE *fp);
void htput_ndb_address(FILE *fp);
void htput_header1_text(FILE *fp,const char *text);
void htput_header1_text_with_icon(FILE *fp, const char *text,  const char *icon);
void htput_header2_text(FILE *fp,const char *text);
void htput_header2_text_with_icon(FILE *fp, const char *text,  const char *icon);
void htput_header3_text(FILE *fp,const char *text);
void htput_header3_text_with_icon(FILE *fp, const char *text,  const char *icon);
void htput_paragraph_break(FILE *fp);
void htput_line_break(FILE *fp);
void htput_text(FILE *fp,const char *text);
void htput_centered_text(FILE *fp,const char *text);
void htput_centered_menu_text(FILE *fp,const char *name);
void htput_centered_menu_file_item(FILE *fp,const char *file, const char *item);
void htput_open_definition_list(FILE *fp);
void htput_def_item1(FILE *fp, const char *subpath, const char *file, const char *item, const char *def);
void htput_close_definition_list(FILE *fp);
void htput_open_unordered_list(FILE *fp);
void htput_list_item1(FILE *fp, const char *subpath, const char *file, const char *item);
void htput_close_unordered_list(FILE *fp);
void htput_pull_down_selection(FILE *fp, const char *label, const char *tag, char **items, int nitems);
void htput_scrolled_selection(FILE *fp,const char *label, const char *tag, char **items, 
			      int nitems, int ndisplay);
void htput_selection_navigator(FILE *fp, long flags);
void htput_open_center(FILE *fp);
void htput_close_center(FILE *fp);
void htput_formatted_section(FILE *fp, const char *section, const char *text);
void htput_formatted_section_with_icon(FILE *fp, const char *section, const char *text, const char *icon);
void htput_example_section(FILE *fp, const std::string& description, const char *text, int numb);
void htput_example_section_with_icon(FILE *fp, const std::string& description, const char *text, int numb, const char *icon);
void htput_open_preformat(FILE *fp);
void htput_close_preformat(FILE *fp);
void ht_strip_nl(char *string);

void htput_open_table(FILE *fp);
void htput_open_table_with_border(FILE *fp);
void htput_close_table(FILE *fp);
void htput_open_row(FILE *fp);
void htput_close_row(FILE *fp);
void htput_table_col_text(FILE *fp, const char *text);
void htput_table_col_text_left(FILE *fp, const char *text);
void htput_table_col_formatted_text(FILE *fp, const char *text);
void htput_table_col_url(FILE *fp, const char *subpath, const char *file, const char *item);
void htput_table_col_header(FILE *fp, const char *text);
void htput_labeled_ref1(FILE *fp, const char *subpath, const char *file, const char *label, const char *item);
void htput_labeled_ref1_with_icon(FILE *fp, const char *subpath, const char *file, const char *label, const char *item, const char *icon);
void htput_labeled_text(FILE *fp, const char *label, const char *text);
void htput_labeled_text_with_icon(FILE *fp, const char *label, const char *text, const char *icon);
void htput_label_with_icon(FILE *fp, const char *label,  const char *icon);

void htput_centered_label(FILE *fp,const char *label);


