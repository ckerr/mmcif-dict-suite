# Top level Makefile

UTIL_LOC = https://svn-dev.wwpdb.org/svn-rcsb/build/util
UTIL_MODULE = util
CHECKOUT_SCRIPT = checkout.sh
DIFF_SCRIPT = diff.sh
COMPILE_SCRIPT = compile.sh
CLEAN_SCRIPT = clean.sh
TEST_SCRIPT = test.sh
CLEAN_TEST_SCRIPT = clean_test.sh
DOC_SCRIPT = doc.sh
CLEAN_DOC_SCRIPT = clean_doc.sh
EXPORT_SCRIPT = export.sh

all: compile

build: checkout compile

clean: clean_build clean_test clean_doc

checkout:
	@echo 
	@echo -------- Getting version Latest of module util --------
	@svn co $(UTIL_LOC)/trunk $(UTIL_MODULE) 
	@sh -c 'cd ./$(UTIL_MODULE); ./$(CHECKOUT_SCRIPT)'

diff:
	@sh -c 'cd ./$(UTIL_MODULE); ./$(DIFF_SCRIPT)'

compile:
	@sh -c 'cd ./$(UTIL_MODULE); ./$(COMPILE_SCRIPT)'

debug:
	@sh -c 'cd ./$(UTIL_MODULE); ./$(COMPILE_SCRIPT) debug'

clean_build: 
	@sh -c 'cd ./$(UTIL_MODULE); ./$(CLEAN_SCRIPT)'

#test: compile
#	@sh -c 'cd ./$(UTIL_MODULE); ./$(TEST_SCRIPT)'

test: test-all

clean_test: 
	@sh -c 'cd ./$(UTIL_MODULE); ./$(CLEAN_TEST_SCRIPT)'

###
	@cd html; rm -rf *.dic *.log
	@cd sdb; rm -f *.sdb *.log
	@cd odb; rm -f *.odb *.log
	@cd xml_v40; rm -f *.log *.xsd *.xml
	@cd mmcif; rm -f *.dic
	@cd test; rm -f exectime.txt 
###

doc:
	@sh -c 'cd ./$(UTIL_MODULE); ./$(DOC_SCRIPT)'

clean_doc:
	@sh -c 'cd ./$(UTIL_MODULE); ./$(CLEAN_DOC_SCRIPT)'

export: clean
	@sh -c 'cd ./$(UTIL_MODULE); ./$(EXPORT_SCRIPT)'

###
###  This is full list of dictionaries routinely built -- 
###
DATADIRS = mmcif_ddl mmcif_std mmcif_pdbx_v40

# 
#  Special target that are under development and are frequently rebuilt for  testing
DATADIRS_DEV =  mmcif_pdbx_v5_next


everything:  compile sdb odb html xml_v40

everything_v40:  compile sdb odb html xml_v40

test-all:
	@sh -c 'cd ./test; ./test.sh'

testall: compile sdb odb xml_v40 html
	@sh -c 'cd ./$(UTIL_MODULE); ./$(TEST_SCRIPT)'

sdb:    compile
	@mkdir -p sdb
	@for datadir in $(DATADIRS); do \
		echo " "; \
		echo "------------------------------------------------------------"; \
		echo Building SDB file for  $$datadir; \
		(./bin/CreateDictSdbFile.csh $$datadir) || exit 1; \
	done

odb:    compile
	@mkdir -p odb
	@for datadir in $(DATADIRS); do \
		echo " "; \
		echo "------------------------------------------------------------"; \
		echo Building object file for  $$datadir; \
		(./bin/CreateDictObjFile.csh $$datadir) || exit 1; \
	done
#
html:   compile
	@for datadir in $(DATADIRS); do \
		echo " "; \
		echo "------------------------------------------------------------"; \
		echo Building HTML translation for  $$datadir; \
		(./bin/DictToHTML.csh $$datadir) || exit 1; \
	done
#

xml_v40:    compile
	@mkdir -p xml_v40
	@for datadir in $(DATADIRS); do \
		echo " "; \
		echo "------------------------------------------------------------"; \
		echo Building xml v4.0 schema file for  $$datadir; \
		(./bin/Dict2XMLSchema.csh $$datadir v40) || exit 1; \
	done

sdb_next:    compile
	@mkdir -p sdb
	@for datadir in $(DATADIRS_DEV); do \
		echo " "; \
		echo "------------------------------------------------------------"; \
		echo Building SDB file for  $$datadir; \
		(./bin/CreateDictSdbFile.csh $$datadir) || exit 1; \
	done
