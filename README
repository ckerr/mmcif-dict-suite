
                    Installation and Usage Notes for the
                             Dictionary Suite



1. Installation

    Uncompress and unbundle the distribution using the following command:

    zcat mmcif-dict-suite-vX.XXX-prod-src.tar.gz | tar -xf -

    The result of this command is a subdirectory
    mmcif-dict-suite-vX.XXX-prod-src in the current directory. It contains
    subdirectories of various source and data modules.


2.  Building the Suite

    Position in the mmcif-dict-suite-vX.XXX-prod-src directory and run "make"
    command:

    cd mmcif-dict-suite-vX.XXX-prod-src
    make

    All the built executables will be placed in the
    "mmcif-dict-suite-vX.XXX-src/bin" subdirectory.


3.  Getting the latest version of PDBx dictionary

    Execute the following commands in order to get the latest version of
    PDBx dictionary and build its binary equivalent SDB file, that is used
    for other purposes, including CIF checking.

    cd mmcif-dict-suite-vX.XXX-prod-src
    mkdir PDBx
    cd PDBx
    wget http://mmcif.pdb.org/dictionaries/ascii/mmcif_pdbx_v40.dic
    ../bin/DictToSdb -ddlFile ../dicts/dict-mmcif_ddl/mmcif_ddl.dic \
      -dictFile mmcif_pdbx_v40.dic -dictSdbFile mmcif_pdbx_v40.sdb

    If errors are found, parsing errors are stored in the file
    mmcif_pdbx_v40.dic-parser.log and validation errors, against the DDL,
    are stored in the file mmcif_pdbx_v40.dic-diag.log 
 

4.  Validating a PDBx file against PDBx dictionary

    This is an example of PDBx file validation, of entry 3q45, against the
    PDBx dictionary.

    cd mmcif-dict-suite-vX.XXX-prod-src
    wget ftp://ftp.wwpdb.org/pub/pdb/data/structures/divided/mmCIF/q4/3q45.cif.gz
    gunzip 3q45.cif.gz
    ./bin/CifCheck -f 3q45.cif -dictSdb PDBx/mmcif_pdbx_v40.sdb

    If errors are found, parsing errors are stored in the file
    3q45.cif-parser.log and validation errors, against the dictionary,
    are stored in the file 3q45.cif-diag.log.


5.  Other Usages

    Position in the mmcif-dict-suite-vX.XXX-prod-src directory and execute
    "make everything_v40":

    cd mmcif-dict-suite-vX.XXX-prod-src
    make everything_v40

    This command will create serialized dictionary files (SDB) and store them
    in "sdb" directory, create dictionary object files (ODB) and store them in
    "odb" directory and convert all the dictionaries to the XML and HTML
    formats, and store the converted files in "xml_v40" and "html" directories.

