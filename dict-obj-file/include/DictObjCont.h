/*
FILE:     DictObjCont.h
*/
/*
VERSION:  2.250
*/
/*
DATE:     10/21/2013
*/
/*
  Comments and Questions to: sw-help@rcsb.rutgers.edu
*/
/*
COPYRIGHT 1999-2013 Rutgers - The State University of New Jersey

This software is provided WITHOUT WARRANTY OF MERCHANTABILITY OR
FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, EXPRESS OR
IMPLIED.  RUTGERS MAKE NO REPRESENTATION OR WARRANTY THAT THE
SOFTWARE WILL NOT INFRINGE ANY PATENT, COPYRIGHT OR OTHER
PROPRIETARY RIGHT.

The user of this software shall indemnify, hold harmless and defend
Rutgers, its governors, trustees, officers, employees, students,
agents and the authors against any and all claims, suits,
losses, liabilities, damages, costs, fees, and expenses including
reasonable attorneys' fees resulting from or arising out of the
use of this software.  This indemnification shall include, but is
not limited to, any and all claims alleging products liability.
*/
/*
               RCSB PDB SOFTWARE LICENSE AGREEMENT

BY CLICKING THE ACCEPTANCE BUTTON OR INSTALLING OR USING 
THIS "SOFTWARE, THE INDIVIDUAL OR ENTITY LICENSING THE  
SOFTWARE ("LICENSEE") IS CONSENTING TO BE BOUND BY AND IS 
BECOMING A PARTY TO THIS AGREEMENT.  IF LICENSEE DOES NOT 
AGREE TO ALL OF THE TERMS OF THIS AGREEMENT
THE LICENSEE MUST NOT INSTALL OR USE THE SOFTWARE.

1. LICENSE AGREEMENT

This is a license between you ("Licensee") and the Protein Data Bank (PDB) 
at Rutgers, The State University of New Jersey (hereafter referred to 
as "RUTGERS").   The software is owned by RUTGERS and protected by 
copyright laws, and some elements are protected by laws governing 
trademarks, trade dress and trade secrets, and may be protected by 
patent laws. 

2. LICENSE GRANT

RUTGERS grants you, and you hereby accept, non-exclusive, royalty-free 
perpetual license to install, use, modify, prepare derivative works, 
incorporate into other computer software, and distribute in binary 
and source code format, or any derivative work thereof, together with 
any associated media, printed materials, and on-line or electronic 
documentation (if any) provided by RUTGERS (collectively, the "SOFTWARE"), 
subject to the following terms and conditions: (i) any distribution 
of the SOFTWARE shall bind the receiver to the terms and conditions 
of this Agreement; (ii) any distribution of the SOFTWARE in modified 
form shall clearly state that the SOFTWARE has been modified from 
the version originally obtained from RUTGERS.  

2. COPYRIGHT; RETENTION OF RIGHTS.  

The above license grant is conditioned on the following: (i) you must 
reproduce all copyright notices and other proprietary notices on any 
copies of the SOFTWARE and you must not remove such notices; (ii) in 
the event you compile the SOFTWARE, you will include the copyright 
notice with the binary in such a manner as to allow it to be easily 
viewable; (iii) if you incorporate the SOFTWARE into other code, you 
must provide notice that the code contains the SOFTWARE and include 
a copy of the copyright notices and other proprietary notices.  All 
copies of the SOFTWARE shall be subject to the terms of this Agreement.  

3. NO MAINTENANCE OR SUPPORT; TREATMENT OF ENHANCEMENTS 

RUTGERS is under no obligation whatsoever to: (i) provide maintenance 
or support for the SOFTWARE; or (ii) to notify you of bug fixes, patches, 
or upgrades to the features, functionality or performance of the 
SOFTWARE ("Enhancements") (if any), whether developed by RUTGERS 
or third parties.  If, in its sole discretion, RUTGERS makes an 
Enhancement available to you and RUTGERS does not separately enter 
into a written license agreement with you relating to such bug fix, 
patch or upgrade, then it shall be deemed incorporated into the SOFTWARE 
and subject to this Agreement. You are under no obligation whatsoever 
to provide any Enhancements to RUTGERS or the public that you may 
develop over time; however, if you choose to provide your Enhancements 
to RUTGERS, or if you choose to otherwise publish or distribute your 
Enhancements, in source code form without contemporaneously requiring 
end users or RUTGERS to enter into a separate written license agreement 
for such Enhancements, then you hereby grant RUTGERS a non-exclusive,
royalty-free perpetual license to install, use, modify, prepare
derivative works, incorporate into the SOFTWARE or other computer
software, distribute, and sublicense your Enhancements or derivative
works thereof, in binary and source code form.

4. FEES.  There is no license fee for the SOFTWARE.  If Licensee
wishes to receive the SOFTWARE on media, there may be a small charge
for the media and for shipping and handling.  Licensee is
responsible for any and all taxes.

5. TERMINATION.  Without prejudice to any other rights, Licensor
may terminate this Agreement if Licensee breaches any of its terms
and conditions.  Upon termination, Licensee shall destroy all
copies of the SOFTWARE.

6. PROPRIETARY RIGHTS.  Title, ownership rights, and intellectual
property rights in the Product shall remain with RUTGERS.  Licensee 
acknowledges such ownership and intellectual property rights and will 
not take any action to jeopardize, limit or interfere in any manner 
with RUTGERS' ownership of or rights with respect to the SOFTWARE.  
The SOFTWARE is protected by copyright and other intellectual 
property laws and by international treaties.  Title and related 
rights in the content accessed through the SOFTWARE is the property 
of the applicable content owner and is protected by applicable law.  
The license granted under this Agreement gives Licensee no rights to such
content.

7. DISCLAIMER OF WARRANTY.  THE SOFTWARE IS PROVIDED FREE OF 
CHARGE, AND, THEREFORE, ON AN "AS IS" BASIS, WITHOUT WARRANTY OF 
ANY KIND, INCLUDING WITHOUT LIMITATION THE WARRANTIES THAT IT 
IS FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE 
OR NON-INFRINGING.  THE ENTIRE RISK AS TO THE QUALITY AND 
PERFORMANCE OF THE SOFTWARE IS BORNE BY LICENSEE.  SHOULD THE 
SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, THE LICENSEE AND NOT 
LICENSOR ASSUMES THE ENTIRE COST OF ANY SERVICE AND REPAIR.  
THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF 
THIS AGREEMENT.  NO USE OF THE PRODUCT IS AUTHORIZED HEREUNDER 
EXCEPT UNDER THIS DISCLAIMER.

8. LIMITATION OF LIABILITY.  TO THE MAXIMUM EXTENT PERMITTED BY
APPLICABLE LAW,  IN NO EVENT WILL LICENSOR BE LIABLE FOR ANY 
INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING 
OUT OF THE USE OF OR INABILITY TO USE THE SOFTWARE, INCLUDING, 
WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK 
STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL 
OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF ADVISED OF THE
POSSIBILITY THEREOF. 
*/


/*!
** \file DictObjCont.h
**
** \brief Header file for ObjCont, ItemObjCont and DictObjCont classes.
*/


#ifndef DICTOBJCONT_H
#define DICTOBJCONT_H


#include "mapped_ptr_vector.h"
#include "mapped_ptr_vector.C"

#include "DictObjContInfo.h"
#include "DicFile.h"


/**
**  \class ObjCont
**
**  \brief Public class that represents a generic object container.
**
**  This class represents a generic object container of attributes. It is
**  to be used directly or as a base class for non-generic object containers.
**  This class provides methods for retrieving its attributes and
**  printing its content.
*/
class ObjCont
{
  public:
    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    ObjCont(Serializer& ser, DicFile& dicFile, const string& blockName,
      const string& id, const ObjContInfo& objContInfo);

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    virtual ~ObjCont();

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    void Init();

    /**
    **  Must stay in public API.
    */
    const string& GetName() const;

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    virtual void Read(UInt32 which, unsigned int Index = 0);

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    virtual UInt32 Write();

    /**
    **  Retrieves a constant reference to the vector of values of the
    **  object container attribute, which is specified with a category name
    **  and an item name.
    **
    **  \param[in] catName - category name
    **  \param[in] itemName - item name
    **
    **  \return Constant reference to the vector of attribute values.
    **
    **  \pre Category with name \e catName and item with name \e itemName
    **    must be present
    **
    **  \post None
    **
    **  \exception NotFoundException - if category with name \e catName
    **    or item with name \e itemName does not exist
    */
    const vector<string>& GetAttribute(const string& catName,
      const string& itemName) const;

    /**
    **  Prints the content of the object container.
    **
    **  \param: None 
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void Print() const;

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    void SetVerbose(bool verbose);

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    virtual void Build();

  protected:
    Serializer& _ser;

    DicFile& _dicFile;

    const ObjContInfo& _objContInfo;

    string _blockName;
    string _id; 

    bool _verbose;

    vector<UInt32> _index;

    vector<vector<vector<string> > > _itemsStore;

    virtual void BuildItems(vector<vector<string> >& combo,
      const unsigned int configIndex);
    void BuildItems(vector<vector<string> >& combo,
      const unsigned int configIndex, const string& value);

  private:
    void ReadItem(const pair<unsigned int, unsigned int>& indexPair,
      unsigned int Index);
};


/**
**  \class ItemObjCont
**
**  \brief Private class that represents an item object container.
**
**  This class represents an item object container, i.e., an object
**  container of type "item". In addition to ObjCont features, this class
**  adds support for item decendents.
*/
class ItemObjCont : public ObjCont
{
  public:
    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    ItemObjCont(Serializer& ser, DicFile& dicFile,
      const string& blockName, const string& itemName);

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    ~ItemObjCont();

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    void Build();

  private:
    vector<string> _decendency;

    void GetItemDecendency();

    void BuildItems(vector<vector<string> >& combo,
      const unsigned int configIndex);
};


/**
**  \class DictObjCont
**
**  \brief Public class that represents a dictionary object container.
**
**  This class represents a dictionary object container, i.e., an object
**  container of type "dictionary". A dictionary object container is a
**  container of its attributes and of objects of type: item, sub-category
**  and category. In addition to ObjCont features, this class has a method
**  to get references to other object containers that it contains.
*/
class DictObjCont : public ObjCont
{
  public:
    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    DictObjCont(Serializer& ser, DicFile& dicFile,
      const string& blockName);

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    ~DictObjCont();

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    void Build();

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    UInt32 Write();

    /**
    **  Utility method, not part of users public API, and will soon be
    **  removed.
    */
    void Read(UInt32 which, unsigned int Index = 0);

    /**
    **  Retrieves a reference to the generic object container, which is
    **  specified with its name and its type.
    **
    **  \param[in] contName - object container name
    **  \param[in] objContInfo - reference to the object container
    **    information, that defines object container's type. It can have the
    **    following values: \n
    **    RcsbItem - indicates that the object container is of type "item" \n
    **    RcsbSubcat - indicates that the object container is of type
    **      "sub-category" \n
    **    RcsbCat - indicates that the object container is of type "category"
    **
    **  \return Reference to the generic object container
    **
    **  \pre Object container with name \e contName must be present
    **
    **  \post None
    **
    **  \exception NotFoundException - if object container with name
    **    \e contName does not exist
    */
    const ObjCont& GetObjCont(const string& contName,
      const ObjContInfo& objContInfo) const;

    /**
    **  Prints the content of the object container, which includes its
    **  attributes and the content of all the object containers that it
    **  contains.
    **
    **  \param: None 
    **
    **  \return None
    **
    **  \pre None
    **
    **  \post None
    **
    **  \exception: None
    */
    void Print();

  private:
    mutable mapped_ptr_vector<ObjCont> _items;
    mutable mapped_ptr_vector<ObjCont> _subcategories;
    mutable mapped_ptr_vector<ObjCont> _categories;

    DictObjCont(const DictObjCont& dictObjCont);
    DictObjCont& operator=(const DictObjCont& inDictObjCont);

    UInt32 WriteContLocations(const vector<UInt32>& indices);

    void BuildContainers(unsigned int index, const string& catName,
      const string& itemName, mapped_ptr_vector<ObjCont>& containers);

    void BuildItems(vector<vector<string> >& combo,
      const unsigned int configIndex);

    ObjCont& GetContainers(const string& contName,
      mapped_ptr_vector<ObjCont>& containers, const ObjContInfo& objContInfo)
      const;

    void PrintContainers(const string& catName,
      const string& itemName, const ObjContInfo& objContInfo);
};


#endif // DICTOBJCONT_H

