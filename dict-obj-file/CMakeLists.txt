add_library(dict_obj_file STATIC
            src/DictDataInfo.C
            src/DictObjCont.C
            src/DictObjContInfo.C
            src/DictObjFile.C
            src/DictParentChild.C
)

target_include_directories(dict_obj_file PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
target_link_libraries(dict_obj_file
                      PUBLIC cif_file cif_common
                      PRIVATE cif_file_util
)

add_executable(DictObjFileCreator src/DictObjFileCreator.C)
target_link_libraries(DictObjFileCreator PRIVATE dict_obj_file)

add_executable(DictObjFileReader src/DictObjFileReader.C)
target_link_libraries(DictObjFileReader PRIVATE dict_obj_file)

add_executable(DictObjFileSelectiveReader src/DictObjFileSelectiveReader.C)
target_link_libraries(DictObjFileSelectiveReader PRIVATE dict_obj_file)
